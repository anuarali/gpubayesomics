#######################################################################################################################
## This function fits the linear regression model                                                                    ##
## dependent variable is gene expression and explanatory variable is methylation probe                               ##
## methylation probes with significant coefficient, R^2 > threshold and normally distributed residuals are returned  ##
#######################################################################################################################

lm_meth <- function(ge_mat, meth_mat, gene, meth_probes, r_squared_thres, p_val_thres)
{
  meth_probes_sig <- c()
  if(length(meth_probes)>0)
  {
    for(f in c(1:length(meth_probes)))
    {
      res <- lm(ge_mat[,gene] ~ meth_mat[,meth_probes[f]])
      if(nrow(summary(res)$coefficients)>1)
      {
        cond1 <- summary(res)$coefficients[2,"Pr(>|t|)"] < p_val_thres
        cond2 <- summary(res)$r.squared > r_squared_thres
        cond3 <- shapiro.test(summary(res)$resid)$p.value > 0.1
        if(cond1 & cond2 & cond3)
        {
          meth_probes_sig <- c(meth_probes_sig, meth_probes[f])
        } # end if(cond1 & cond2 & cond3)
      } # end if(nrow(summary(res)$coefficients)>1)
    } # end for f
  } # end if(length(meth_probes)>0)
  
  return(meth_probes_sig)
}
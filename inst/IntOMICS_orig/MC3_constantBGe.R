#' @export
MC3_constantBGe <- function(source_net, omics, layers_def, B_prior_mat, beta.source, partition_func_UB_beta_source, parent_set_combinations, BGe_score_all_configs_node, annot)
{
  ge_nodes <- rownames(source_net$adjacency)[regexpr("ENTREZ", rownames(source_net$adjacency))>0]
  vec <- 1:length(c(source_net$adjacency))
  vec <- vec[c(B_prior_mat)>0]
  edge_proposal_res <- edge_proposal(net = source_net$adjacency, candidates = vec, layers_def = layers_def, ge_nodes = ge_nodes, omics = omics, B_prior_mat = B_prior_mat)
  
  while(edge_proposal_res$no_action | !is.acyclic(edge_proposal_res$net))
  {
    vec <- vec[vec!=edge_proposal_res$edge]
    edge_proposal_res <- edge_proposal(net = source_net$adjacency, candidates = vec, layers_def = layers_def, ge_nodes = ge_nodes, omics = omics, B_prior_mat = B_prior_mat)
  } # end while(edge_proposal_res$no_action | !is.acyclic(edge_proposal_res$net))

  # source_net_adjacency <- edge_proposal_res$net
  nbhd.size <- neighborhood_size(net = edge_proposal_res$net, layers_def = layers_def, B_prior_mat = B_prior_mat, omics = omics)
  
  ## Prior probability P(G)
  energy <- sum(epsilon(net = edge_proposal_res$net, B_prior_mat = B_prior_mat))
  prior <- (-beta.source$value*energy) - partition_func_UB_beta_source
  
  likelihood_part <- source_net$BGe + prior
  # edge_move <- edge_proposal_res$edge_move
  
  return(list(adjacency = edge_proposal_res$net, nbhd.size = nbhd.size, proposal.distr = c(), energy = energy, prior = prior, BGe = source_net$BGe, likelihood_part = likelihood_part, likelihood = c(), acceptance = c(), edge_move = edge_proposal_res$edge_move))
}

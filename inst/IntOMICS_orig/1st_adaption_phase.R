#' ##########################
#' ### 1st adaption phase ###
#' ### we change the variance of the proposal distribution to achieve the MC acceptance rate of 0.44
#' @export
first_adapt_phase <- function(seed1, omics, B_prior_mat, energy_all_configs_node, len, layers_def, prob_mbr, BGe_score_all_configs_node, parent_set_combinations, annot, imprvd_data)
{
  init.net1 <- init.net.mcmc(omics = omics, seed = seed1, layers_def = layers_def, B_prior_mat = B_prior_mat)
  first.adapt.phase_net <- source_net_def(init.net.mcmc.output = init.net1,
                                omics = omics,
                                parent_set_combinations = parent_set_combinations,
                                BGe_score_all_configs_node = BGe_score_all_configs_node,
                                B_prior_mat = B_prior_mat,
                                layers_def = layers_def,
                                energy_all_configs_node = energy_all_configs_node,
                                len = len,
                                imprvd_data=imprvd_data)
  first.adapt.phase_net$seed <- seed1
  set.seed(first.adapt.phase_net$seed)

  # For every 100 iteration, we calculate the beta acceptance rate of the past 100 iterations
  # and we add = 0.05 to log(len) if the acceptance rate is higher than 0.44, and subtract from log(len) if the acceptance rate is lower or equal than 0.44.
  # we do this until the acceptance rate for beta falls between 0.28 and 0.60.
  first.adapt.phase_net <- acceptance_check(first.adapt.phase_net = first.adapt.phase_net, round_check = 100, last_iter_check = 100, prob_mbr = prob_mbr, layers_def = layers_def, parent_set_combinations = parent_set_combinations, BGe_score_all_configs_node = BGe_score_all_configs_node, omics = omics, annot = annot, imprvd_data=imprvd_data)

  # We run 100 more iterations with same len, which have made the acceptance rate to fall between 0.28 and 0.60,
  # and monitor the acceptance rate for the past 200 iterations.
  # If the beta acceptance rate falls outside of 0.28 and 0.60, then we adjust log(len) for every 100 iterations and monitor the acceptance rate for the past 200 iterations until the acceptance rate comes between 0.28 and 0.60.
  first.adapt.phase_net <- acceptance_check(first.adapt.phase_net = first.adapt.phase_net, round_check = 100, last_iter_check = 200, prob_mbr = prob_mbr, layers_def = layers_def, parent_set_combinations = parent_set_combinations, BGe_score_all_configs_node = BGe_score_all_configs_node, omics = omics, annot = annot, imprvd_data=imprvd_data)

  # We run 200 more iterations with same len, which have made the acceptance rate to fall between 0.28 and 0.60,
  # and monitor the acceptance rate for the past 400 iterations.
  # If the beta acceptance rate falls outside of 0.28 and 0.60, then we adjust log(len) for every 200 iterations and monitor the acceptance rate for the past 400 iterations until the acceptance rate comes between 0.28 and 0.60.
  first.adapt.phase_net <- acceptance_check(first.adapt.phase_net = first.adapt.phase_net, round_check = 200, last_iter_check = 400, prob_mbr = prob_mbr, layers_def = layers_def, parent_set_combinations = parent_set_combinations, BGe_score_all_configs_node = BGe_score_all_configs_node, omics = omics, annot = annot, imprvd_data=imprvd_data)
  # We stop the chain and save len.
  return(first.adapt.phase_net)
}

###############################################################################################
## This function computes for each GE/CNV node its energy over all parent set configurations ##
## Also empty parent set is included                                                         ##
###############################################################################################
# int_node is character, the name of given node
# all_parents_config is matrix, each column indicates parents of given int_node

energy_function_node_specific <- function(all_parents_config, B_prior_mat, int_node)
{
  epsilon <- apply(all_parents_config,2,FUN=function(z)
    if(is.na(z[1]))
    {
      sum(B_prior_mat[,int_node])
    } else {
      sum(1-B_prior_mat[z,int_node]) + sum(B_prior_mat[-match(z,rownames(B_prior_mat)),int_node])
    })
  return(epsilon)
}


########################################
## biological prior matrix definition ##
########################################

B_prior_mat <- function(omics, PK, layers_def, TFtargs, annot, lm_METH, r_squared_thres, p_val_thres, TFBS_belief, nonGE_belief, woPKGE_belief)
{
  
  if(any(regexpr("ENTREZID:",colnames(omics[[layers_def$omics[1]]]))<0))
  {
  	stop("Gene names in GE matrix are not in the correct form. Please, use ENTREZID:XXXX.")
  } # end if(regexpr("ENTREZID:",colnames(omics[[layers_def$omics[1]]]))<0)
  
  if(!is.null(TFtargs))
  {
    if(any(regexpr("ENTREZID:",colnames(TFtargs))<0))
    {
      message("Gene names in TFtargs are not in the correct form. Please, use ENTREZID:XXXX.")
    } # end if(any(regexpr("ENTREZID:",colnames(TFtargs))<0))
  } # end if(!is.null(TFtargs))
  
  if(is.null(PK))
  {
      message("There in no PK. Please, consider adding PK to increase the IntOMICS prediction accuracy.")
  } else {
    if(is.list(omics))
    {
      if(length(intersect(c(PK$src_entrez,PK$dest_entrez),unlist(mapply(colnames,omics))))==0)
      {
        message("There are no valid PK interactions. Please, check if PK fits omics features.")
      } # end if(length(intersect(c(PK$src_entrez,PK$dest_entrez),c(mapply(colnames,omics))))==0)
    } else {
      if(length(intersect(c(PK$src_entrez,PK$dest_entrez),colnames(omics)))==0)
      {
        message("There are no valid PK interactions. Please, check if PK fits omics features.")
      } # end if(length(intersect(c(PK$src_entrez,PK$dest_entrez),c(mapply(colnames,omics))))==0)
    } # end if else (is.list(omics))
  } # end if else if(is.null(PK))
  
  
  if(!all(sort(names(omics))==sort(layers_def$omics)))
  {
    stop("Names of the list 'omics' does not match omics names in the 'layers_def'.")
  } # end if(!all(order(names(omics))==order(layers_def$omics)))
  
  # the first value indicates the belief of present nodes, the second value indicates the belief of absent nodes
  pk_belief <- c(1,0)

  ## interactions from the last layer (should be GE)
  features <- colnames(omics[[layers_def$omics[1]]])

  # woPKGE_belief value denotes that we have not any information about the presence of an edge
  B_layer_max <- matrix(woPKGE_belief, ncol=length(features), nrow=length(features), dimnames=list(features,features))
  
  # B_ij = 0.75 we have prior evidence that there is a directed edge pointing from node i to node j in TF targets database
  TFs <- intersect(colnames(TFtargs),rownames(B_layer_max))
  targets <- intersect(rownames(TFtargs),rownames(B_layer_max))
  if(length(TFs)>0 & length(targets)>0)
  {
    TFtargs_spec <- matrix(TFtargs[targets, TFs], nrow = length(targets), dimnames=list(targets,TFs))
    for(j in c(1:ncol(TFtargs_spec)))
    {
      B_layer_max[colnames(TFtargs_spec)[j],names(which(TFtargs_spec[,j]==1))] <- TFBS_belief
    } # end for j
  } # end if(length(TFs)>0 & length(targets)>0)

  # B_ij = 1 we have prior evidence that there is a directed edge pointing from node i to node j
  PK_present <- PK[PK$edge_type=="present",]
  PK_absent <- PK[PK$edge_type=="absent",]
  
  for(i in c(1:nrow(B_layer_max)))
  {
    if (sum(PK_present$src_entrez==rownames(B_layer_max)[i]) != 0)
    {
      B_layer_max[rownames(B_layer_max)[i],intersect(PK_present[PK_present$src_entrez==rownames(B_layer_max)[i],"dest_entrez"],rownames(B_layer_max))] <- pk_belief[1]
    } # end if sum...
    
    if (sum(PK_absent$src_entrez==rownames(B_layer_max)[i]) != 0)
    {
      B_layer_max[rownames(B_layer_max)[i],intersect(PK_absent[PK_absent$src_entrez==rownames(B_layer_max)[i],"dest_entrez"],rownames(B_layer_max))] <- pk_belief[2]
    } # end if sum...
    
  } # end for i...
  B <- B_layer_max
  diag(B) <- 0
  new_annot <- list()
  
  omics_meth_original <- matrix(nrow = 0, ncol = 0)
  ## interactions from other layers
  if(length(layers_def$omics)>1)
  {
    for(j in c(2:length(layers_def$omics)))
    {
      features_lower <- colnames(omics[[layers_def$omics[j]]])
      if(any(regexpr("entrezid:",colnames(omics[[layers_def$omics[j]]]))>0)==TRUE)
      {
        B_layer_lower <- matrix(0, ncol=ncol(B),
                                nrow=length(features_lower), 
                                dimnames=list(features_lower[match(colnames(B), toupper(features_lower), nomatch=0)],colnames(B)))
        # CNV data: all possible interactions are set to 0.75
        diag_sim <- match(toupper(rownames(B_layer_lower)),colnames(B_layer_lower))
        B_layer_lower[cbind(1:length(features_lower), diag_sim)] <- nonGE_belief
        B_layer1 <- matrix(0, ncol=length(features_lower), nrow=nrow(B)+length(features_lower), 
                           dimnames=list(c(rownames(B), features_lower), rownames(B_layer_lower)))
        B <- cbind(rbind(B,B_layer_lower),B_layer1)
      } else {
        annot <- annot[intersect(names(annot),colnames(omics[[layers_def$omics[1]]]))]
        annot <- lapply(annot, FUN=function(s) intersect(s,colnames(omics[[layers_def$omics[j]]])))
        
        omics_meth_original <- omics[[layers_def$omics[j]]]
        # Methylation data transformation:
        omics[[layers_def$omics[j]]] <- apply(omics[[layers_def$omics[j]]], 2, FUN =function(column) orderNorm(column)$x.t)
        
        if(lm_METH)
        {
          new_annot <- lapply(seq_along(annot), function(list) lm_meth(omics[[layers_def$omics[1]]], omics[[layers_def$omics[j]]], names(annot)[[list]], annot[[list]], r_squared_thres = r_squared_thres, p_val_thres = p_val_thres))
          names(new_annot) <- names(annot)
        } else {
          new_annot <- annot
        } # end if(lm_METH)
        
        if(sum(!mapply(is.null,new_annot))!=0)
        {
         new_annot <- new_annot[!mapply(is.null,new_annot)]
         features_lower <- unlist(new_annot)
         B_layer_lower <- matrix(0, ncol=ncol(B), 
                                 nrow=length(features_lower), 
                                 dimnames=list(features_lower,colnames(B)))
         # METH data: all possible interactions from annot are set to 0.75
         for(a in c(1:length(new_annot)))
         {
           B_layer_lower[intersect(features_lower,new_annot[[a]]),names(new_annot)[a]] <- nonGE_belief
         } # end for a
         B_layer1 <- matrix(0, ncol=length(features_lower), nrow=nrow(B)+length(features_lower), 
                            dimnames=list(c(rownames(B), features_lower), rownames(B_layer_lower)))
         B <- cbind(rbind(B,B_layer_lower),B_layer1)
         omics[[layers_def$omics[j]]] <- omics[[layers_def$omics[j]]][,unlist(new_annot)]
        } else {
          new_annot <- list()
          omics[[layers_def$omics[j]]] <- matrix(nrow = 0, ncol = 0)
        } # end if else (sum(!mapply(is.null,new_annot))==0)
        # there are some parents of METH_candidates[i] from the IntOMICS network and the METH value has more than 1 factor level
      } # end if else (any(regexpr("entrezid:",colnames(omics[[layers_def$omics[j]]]))>0)==TRUE)
    } # end for j
  } # end if(length(layers_def$omics)>1)
  return(list(B_prior_mat = B, annot = new_annot, omics = omics, omics_meth_original = omics_meth_original))
}

#' @export
parent_sets_sum_scores_childrenX <- function(parent_set_combinations, selected_node, children_selected_node, child_order, dag_tmp_bn, new_parent_set, source_net_adjacency, BGe_score_all_configs_node)
{
  sum_score_unmarked <- c()
  
  if(new_parent_set)
  {
    for(j in child_order)
    {
      descendants <- bnlearn::descendants(x = dag_tmp_bn, node = children_selected_node[j])
      # possible parent sets of selected_node child that contain a node from descendants and that does not contain selected_node
      BGe_marked <- lapply(parent_set_combinations[[children_selected_node[j]]], FUN=function(list) apply(list, 2, FUN=function(column) length(intersect(column, descendants))>0 | !any(column==selected_node)))
      # empty parent set is also possible (it is always possible - it does not contain selected_node)
      BGe_marked[[1]][is.na(parent_set_combinations[[children_selected_node[j]]][[1]])] <- TRUE
      names(BGe_marked) <- paste(as.character(1:length(BGe_marked)),"_",sep="")
      # sample new parent set of selected_node's children_selected_node
      BGe_marked_compressed <- lapply(BGe_marked,FUN=function(list) which(list==FALSE))
      possible_parent_sets_ind <- unlist(BGe_marked_compressed, use.names = TRUE)
      if(length(possible_parent_sets_ind)==0)
      {
        new_parent_set <- NA
        sum_score_unmarked[j] <- NA
      } else if(length(possible_parent_sets_ind)==1)
      {
        sum_score_unmarked[j] <- unlist(Map(function(pos, scores) scores[!pos], BGe_marked, BGe_score_all_configs_node[[children_selected_node[j]]]))
        ind <- as.numeric(unlist(lapply(strsplit(names(possible_parent_sets_ind),"_"),FUN=function(list) list[1])))
        new_parent_set <- parent_set_combinations[[children_selected_node[j]]][[ind]][,possible_parent_sets_ind]
      } else {
        score_unmarked <- unlist(Map(function(pos, scores) scores[!pos], BGe_marked, BGe_score_all_configs_node[[children_selected_node[j]]]))
        new_parent_set_ind <- sample(x = 1:length(possible_parent_sets_ind), size = 1, prob = range01(score_unmarked - sum(score_unmarked)))
        ind <- as.numeric(unlist(lapply(strsplit(names(possible_parent_sets_ind[new_parent_set_ind]),"_"),FUN=function(list) list[1])))
        new_parent_set <- parent_set_combinations[[children_selected_node[j]]][[ind]][,possible_parent_sets_ind[new_parent_set_ind]]
        sum_score_unmarked[j] <- logSumExp(score_unmarked)
      } # end if(length(possible_parent_sets_ind)==0)
      # add the new parent set to dag_tmp
      amat(dag_tmp_bn)[new_parent_set, children_selected_node[j]] <- 1
    } # end for j
    return(list(new_parent_set = new_parent_set, sum_score_unmarked = sum_score_unmarked, dag_tmp_bn = dag_tmp_bn, BGe_marked = BGe_marked))
  } else {
    for(j in child_order)
    {
      descendants <- bnlearn::descendants(x = dag_tmp_bn, node = children_selected_node[j])
      # possible parent sets of selected_node child that contain a node from descendants_child and that does not contain selected_node
      BGe_marked <- lapply(parent_set_combinations[[children_selected_node[j]]], FUN=function(list) apply(list, 2, FUN=function(column) length(intersect(column, descendants))>0 | !any(column==selected_node)))
      # empty parent set is also possible (it is always possible - it does not contain selected_node)
      BGe_marked[[1]][is.na(parent_set_combinations[[children_selected_node[j]]][[1]])] <- TRUE
      names(BGe_marked) <- paste(as.character(1:length(BGe_marked)),"_",sep="")
      # store the sum of unmarked scores
      BGe_marked_compressed <- lapply(BGe_marked,FUN=function(list) which(list==FALSE))
      possible_parent_sets_ind <- unlist(BGe_marked_compressed, use.names = TRUE)
      if(length(possible_parent_sets_ind)==0)
      {
        sum_score_unmarked[j] <- NA
      } else if(length(possible_parent_sets_ind)==1)
      {
        sum_score_unmarked[j] <- unlist(Map(function(pos, scores) scores[!pos], BGe_marked, BGe_score_all_configs_node[[children_selected_node[j]]]))
      } else {
        sum_score_unmarked[j] <- logSumExp(unlist(Map(function(pos, scores) scores[!pos], BGe_marked, BGe_score_all_configs_node[[children_selected_node[j]]])))
      } # end if(length(possible_parent_sets_ind)==0)
      # add the original parent set to the candidate dag
      amat(dag_tmp_bn)[names(which(source_net_adjacency[,children_selected_node[j]]==1)), children_selected_node[j]] <- 1
    } # end for j
    return(list(sum_score_unmarked = sum_score_unmarked, dag_tmp_bn = dag_tmp_bn, BGe_marked = BGe_marked))
  } # end if(new_parent_set)
  
}

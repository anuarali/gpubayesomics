#' ####################################################################################
#' ## Compute the BGe score of given node using precomputed sets of possible parents ##
#' ####################################################################################
#' @export
BGe_node <- function(node, adjacency_matrix, parent_set_combinations, BGe_score_all_configs_node)
{
  parents <- names(which(adjacency_matrix[,node]==1))
  if(length(parents)>0)
  {
    parents_ind <- which(apply(parent_set_combinations[[node]][[length(parents)]],2,FUN=function(column) length(intersect(column,parents))==length(parents)))
    score_node <- BGe_score_all_configs_node[[node]][[length(parents)]][parents_ind]
  } else {
    score_node <- BGe_score_all_configs_node[[node]][[1]][is.na(parent_set_combinations[[node]][[1]])]
  } # end if(length(parents)>0)
  return(score_node)
}
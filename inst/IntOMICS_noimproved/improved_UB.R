###############################################################################################
## This function computes for each GE/CNV node its energy over all parent set configurations ##
## Also empty parent set is included                                                         ##
###############################################################################################
# int_node is character, the name of given node
# all_parents_config is matrix, each column indicates parents of given int_node


imprvd_energy_function_node_specific <- function(all_parents_config, B_prior_mat, int_node)
{
    epsilon <- apply(all_parents_config,2,FUN=function(z)
        if(is.na(z[1]))
        {
            sum(B_prior_mat[,int_node])
        } else {
            sum(1-B_prior_mat[z,int_node]) + sum(B_prior_mat[-match(z,rownames(B_prior_mat)),int_node])
        })
    return(epsilon)
}


imprvd_coefs_matrix <- function(TFBS_belief = 0.75, nonGE_belief = 0.5, woPKGE_belief = 0.5, absent_belief=0, GE_belief=1)
{
    coefs <- data.frame(matrix(0, 1, 5))
    colnames(coefs) <- c("absent", "noPK", "GE", "nonGE", "TF")

    coefs$absent <- absent_belief
    coefs$GE <- GE_belief
    coefs$TF <- TFBS_belief
    coefs$nonGE <- nonGE_belief
    coefs$noPK <- woPKGE_belief

    return(coefs)
}


imprvd_calculate_histogram_of_B_prior <- function(B_prior_mat, coefs)
{
    library(dplyr)

    N <- nrow(B_prior_mat)

    imprvd_get_counts_per_node <- function(col_B, coefs)
    {
        nums <- match(col_B, coefs[1,])  # get indices for every element
        counts <- as.data.frame(table(nums))

        ret <- data.frame(matrix(0, 1, 5))
        colnames(ret) <- colnames(coefs)
        ret[,as.numeric(levels(counts$nums))] <- counts$Freq

        return(ret)
    }

    nums <- do.call("rbind", apply(B_prior_mat, 2, imprvd_get_counts_per_node, coefs=coefs))
    H <- suppressMessages(
        nums %>% group_by_all() %>% summarise(hist_count = n())
    )

    H <- data.frame(H)

    return(H)
}


..imprvd_calculation_log_per_type <- function(combo, beta)
{
    N <- combo[1]
    C <- combo[2]

    if(C == 0 || N == 0)
    {
        return(0)
    }


    K <- 0:N
    choose_term <- choose(N, K)

    term_exp <- N * C  + (1 - 2 * C) * K
    term_exp <- -beta * term_exp
    ret <- sum(choose_term * exp(term_exp))
    ret <- log(ret)

    return(ret)
}

..imprvd_calculation_per_set <- function(row_hist, coefs, beta)
{
    extended_row <- data.frame(matrix(0, nrow=2, ncol=length(row_hist)))
    extended_row[1, 1:length(row_hist)] <- row_hist
    extended_row[2, 1:ncol(coefs)] <- coefs
    count <- row_hist[["hist_count"]]


    res <- apply(extended_row[,1:ncol(coefs)], 2, ..imprvd_calculation_log_per_type, beta=beta)
    res <- sum(res)

    return(res * count)
}

imprvd_log_UB_for_specific_beta <- function(histogram, coefs, beta)
{
    ret <- apply(histogram, 1, ..imprvd_calculation_per_set, coefs=coefs, beta=beta)
    ret <- sum(ret)
    return(ret)
}

printf <- function(...) cat(sprintf(...))

imprvd_sample_init_net <- function(empty_net, seed, B_prior)
{
    if(!missing(seed))
    {
        set.seed(seed)
    }

    is_acyc <- FALSE
    ret_net <- NA

    while(!is_acyc)
    {
        # start by picking the random parent for each node
        pick_random_par_per_node <- function(idx_col)
        {
            col <- B_prior[,idx_col]
            allowed_in_par <- (1:ncol(B_prior))[col != 0]  # take all allowed parents
            ret <- rep(0, length(col))

            if(length(allowed_in_par) > 0)
            {
                picked <- allowed_in_par[sample(length(allowed_in_par), 1)]  # pick 1 of them randomly

                if(runif(1) > 0.5)
                {
                    ret[picked] <- 1
                }
            }

            return(ret)
        }
        ret_net <- sapply(1:ncol(B_prior), pick_random_par_per_node)
        is_acyc <- is.acyclic(ret_net) # if contains cycles -> repeat
    }

    dataset_BND <- BNDataset(data = empty_net,
                             discreteness = rep('d',ncol(empty_net)),
                             variables = c(colnames(empty_net)),
                             node.sizes = rep(2,ncol(empty_net)), starts.from=0)
    net <- BN(dataset_BND)
    dag(net) <- ret_net
    return( suppressMessages(learn.params(net,dataset_BND)) )
}



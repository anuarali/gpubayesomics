####################################################################################################################
## This function computes for each GE/CNV node its energy over all possible parent set configurations             ##
## This energy is used in MCMC sampling to the partition function upper bound computation                         ##
## The BGe score is also pre-computed for each parent set configurations                                          ##
####################################################################################################################
# the resulting partition_func_UB and BGe score are log transformed

pf_UB_est <- function(omics, B_prior_mat, layers_def, annot)
{
  ## PART I: the upper bound of the partition function
  # define all parents configurations
  comb_all <- foreach (i=1:ncol(omics[[layers_def$omics[1]]])) %do% {
    int_node <- colnames(omics[[layers_def$omics[1]]])[i]
    potentials_layer <- intersect(rownames(B_prior_mat)[B_prior_mat[,int_node]>0],colnames(omics[[layers_def$omics[1]]]))
    comb_some <- list()

    ## GE all parents set combinations
    for(rep in c(1:layers_def$fan_in_ge[1]))
    {
        if(length(potentials_layer) < rep)
        {
            comb_some[[rep]] <- matrix(NA, 1, 1)
            next
        }
        comb_some[[rep]] <- combn(potentials_layer,rep)
    } # end for rep

    # add empty parent sets
    comb_some[[layers_def$fan_in_ge[1]+1]] <- matrix(NA,1,1)

    if(length(layers_def$omics)>1)
    {
      ## add CNV for each parents set combination including empty parent sets
      modalities <- layers_def$omics[-1]
      if(any(mapply(FUN=function(mod) any(regexpr("entrezid:",colnames(mod))>0)==TRUE, omics[modalities])) & tolower(int_node) %in% unlist(lapply(omics[modalities],colnames)))
      {
        comb_some[seq(length(comb_some)+1,length.out = length(comb_some))] <- lapply(comb_some,FUN=function(list) rbind(list,tolower(int_node)))
      } # end if(any(mapply(FUN=function(mod) any(regexpr("entrezid:",colnames(mod))>0)==TRUE, omics[modalities])))

      ## is there METH?
      if(any(mapply(omics,FUN=function(list) any(regexpr("entrezid:",colnames(list), ignore.case = TRUE)<0))) & length(annot[[int_node]])>0)
      {
        modality <- names(which(mapply(omics,FUN=function(list) any(regexpr("entrezid:",colnames(list), ignore.case = TRUE)<0))==TRUE))
        max_fan_in <- max(layers_def$fan_in_ge[layers_def$omics==modality],length(annot[[int_node]]), na.rm = TRUE)

        ## only METH all parents set combinations
        comb_some_meth <- list()
        for(rep in c(1:max_fan_in))
          {
            comb_some_meth[[rep]] <- combn(annot[[int_node]],rep)
        } # end for rep

        ## add METH for each parents set combination including empty parent sets
        comb_some_meth_add <- list()
        for(meth_pr in c(1:length(comb_some_meth)))
        {
          comb_some_meth_add <- c(comb_some_meth_add, foreach(a=1:ncol(comb_some_meth[[meth_pr]])) %do% {
            lapply(comb_some, FUN=function(par_def) apply(par_def,2,FUN=function(par_def_col) c(par_def_col, comb_some_meth[[meth_pr]][,a])))
          }) # end foreach(a=1:ncol(comb_some_meth[[meth_pr]])) %do%
        } # end for meth_pr

        comb_some_meth_add <- flattenlist(comb_some_meth_add)
        comb_some <- c(comb_some, comb_some_meth_add)
      } # if if(any(mapply(omics,FUN=function(list) any(regexpr("entrezid:",colnames(list), ignore.case = TRUE)<0))))
    } # end if(length(layers_def$omics)>1)

    # now remove NAs (delete an empty parent set too)
    comb_some <- lapply(comb_some,na.omit)
    # add again an empty parent set

    #comb_some[[1]] <- cbind(comb_some[[1]], NA)
    if(length(potentials_layer) >= 1)
    {
        # now remove NAs (delete an empty parent set too)
        # add again an empty parent set
        comb_some[[1]] <- cbind(comb_some[[1]], NA)
    }
    else{
        comb_some[[1]] <- matrix(NA, 1, 1)
    }

    comb_some <- comb_some[mapply(comb_some,FUN=function(x) nrow(x))!=0]

    parents_config <- list()
    for(l in c(1:max(mapply(comb_some,FUN=function(x) nrow(x)))))
    {
      parents_config[[l]] <- do.call(cbind, comb_some[mapply(comb_some,FUN=function(x) nrow(x))==l])
    } # end for l
    parents_config
  } # end foreach (i=1:ncol(omics[[layers_def$omics[1]]]))
  names(comb_all) <- colnames(omics[[layers_def$omics[1]]])

  if(length(layers_def$omics)>1)
  {
    ## add also CNV/METH nodes and their empty parent set configurations
    comb_all_others <- vector(mode = "list", length = sum(mapply(ncol,omics[setdiff(layers_def$omics,layers_def$omics[1])])))
    comb_all_others <- lapply(comb_all_others, FUN=function(list) list <- matrix(NA))
    names(comb_all_others) <- unlist(mapply(colnames,omics[setdiff(layers_def$omics,layers_def$omics[1])]))
    comb_all <- c(comb_all, comb_all_others)
  } # end if(length(layers_def$omics)>1)


  energy_all_configs_node <- list()
  ## GE
  for(i in c(1:ncol(omics[[layers_def$omics[1]]])))
  {
    energy_all_configs_node[[i]] <- unlist(lapply(comb_all[[i]],FUN=function(list) energy_function_node_specific(list, B_prior_mat, names(comb_all)[i])))
  } # end for i
  ## CNV/METH
  # because we force the CNV/METH nodes to be orphaned, we can only sum the values in the B_prior_mat (now it is 0 because of 0 pk_belief)
  if(length(layers_def$omics)>1)
  {
    for(i in c((ncol(omics[[layers_def$omics[1]]])+1):(ncol(omics[[layers_def$omics[1]]])+sum(mapply(ncol,omics[setdiff(layers_def$omics,layers_def$omics[1])])))))
    {
      energy_all_configs_node[[i]] <- sum(B_prior_mat[,names(comb_all)[i]])
    } # end for i
  } # end if(length(layers_def$omics)>1)

  partition_func_UB <- sum(log(mapply(energy_all_configs_node,FUN=function(x) sum(exp(-0*x)))))

  ## PART II: BGe score for each parent set configuration
  data <- do.call(cbind, omics[mapply(nrow,omics)>0])
  myScore <- scoreparameters_BiDAG_BGe(n = ncol(data), data = data)

  # for each parent set of given node
  n <- ncol(myScore$data)
  BGe_score_list <- list()
  ## GE
  for(i in c(1:ncol(omics[[layers_def$omics[1]]])))
  {
    BGe_score_list[[i]] <- lapply(comb_all[[i]], FUN=function(list) apply(list, 2, FUN=function(column)
      if(is.na(column[1]))
      {
        DAGcorescore(names(comb_all)[i], integer(length = 0), n = myScore$n, param = myScore)
      } else {
        DAGcorescore(names(comb_all)[i], column, n = myScore$n, param = myScore)
      }))
  } # end for(i in c(1:ncol(omics[[layers_def$omics[1]]])))
  ## CNV/METH
  if(length(layers_def$omics)>1)
  {
    for(i in c((ncol(omics[[layers_def$omics[1]]])+1):(ncol(omics[[layers_def$omics[1]]])+sum(mapply(ncol,omics[setdiff(layers_def$omics,layers_def$omics[1])])))))
    {
      BGe_score_list[[i]] <- matrix(DAGcorescore(names(comb_all)[i], integer(length = 0), n = myScore$n, param = myScore))
    } # end for i
  } # end if(length(layers_def$omics)>1)

  names(BGe_score_list) <- names(comb_all)

  return(list(partition_func_UB = partition_func_UB, parents_set_combinations = comb_all, energy_all_configs_node = energy_all_configs_node, BGe_score_all_configs_node = BGe_score_list))
}

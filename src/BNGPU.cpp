#include "BNGPU.h"

#include <iostream>
#include <Rcpp/utils/tinyformat/tinyformat.h>
SEXP BNGPU::addDataTable(
        const SEXP input,
        const CollumnDataEntry row_type,
        const CollumnDataEntry col_type,
        const MatrixFormatType matrix_type
        )
{
    int ret = TYPEOF(input);

    if(!Rf_inherits(input, "data.frame") && !Rf_isMatrix(input))
    {
        Rcpp::stop("Error! Only data frame or matrix allowed as input!");
    }

    Rcpp::CharacterVector column_names = Rcpp::colnames(input);
    Rcpp::CharacterVector row_names = Rcpp::rownames(input);
    return column_names;
    // if(column_names[0] == R_NilValue)
    // {
    //     Rcpp::stop("Error! No collumn names given!")
    // }

    int nrows, ncols;


    if( Rf_inherits( input, "data.frame" )){
        std::cout << "I got data frame!" << std::endl;
        Rcpp::DataFrame df = input;
        nrows = df.nrow();
        ncols = df.ncol();
    }
    else if(Rf_isMatrix(input)){
        std::cout << "I got matrix!" << std::endl;
        Rcpp::NumericMatrix m = input;
        nrows = m.nrow();
        ncols = m.ncol();
    }


    std::cout << "Data input is " << nrows << " x " << ncols << std::endl;
    std::cout << "Column names: ";
    for(int i = 0; i < ncols; i++)
    {
        const char *s;
        s = CHAR(STRING_ELT(column_names[i], 0));
        std::cout << s << " ";
    }
    std::cout << std::endl;
    return column_names;
}





#ifndef DATA_H
#define DATA_H
#include "EnumCollumnDataEntry.h"
#include "EnumMatrixFormatType.h"

#include <vector>
#include <Rcpp.h>

//' Read a data from given path
//' @param path path to the file data
//' @export
// [[Rcpp::export]]
int dataRead(const std::vector<int>& path);



//' Compute some complex algorithm
//' @param path path to the file with input data
//' @export
// [[Rcpp::export]]
int computeAlgorithm(const char* path);


// //' Universal function to read any data input. Parameters specify the table
// //'
// //' @param path path to the file with input data
// // [[Rcpp::export]]
// void addDataTable_path(const char* filepath, const EnumCollumnDataEntry row_type, const EnumCollumnDataEntry col_type, const EnumMatrixFormatType matrix_type);

#endif//DATA_H

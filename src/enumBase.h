#ifndef ENUM_BASE_H
#define ENUM_BASE_H

template <typename E>
class EnumBaseWrapper{
public:
    EnumBaseWrapper(){}
    void setP(E pol){p = pol;}
    E getP(){return p;}
private:
    E p;
};

#endif//ENUM_BASE_H

#include "EnumCollumnDataEntry.h"
#include "EnumMatrixFormatType.h"
#include "BNGPU.h"

#include <Rcpp.h>
using namespace Rcpp;


RCPP_EXPOSED_ENUM_NODECL(CollumnDataEntry)
//RCPP_EXPOSED_CLASS_NODECL(EnumCollumnDataEntry)
RCPP_MODULE(EnumCollumnDataEntry){
    Rcpp::class_<EnumCollumnDataEntry>("EnumCollumnDataEntry")
    .constructor()
    .property("p", &EnumCollumnDataEntry::getP, &EnumCollumnDataEntry::setP)
    ;
}


RCPP_EXPOSED_ENUM_NODECL(MatrixFormatType)
//RCPP_EXPOSED_CLASS_NODECL(EnumMatrixFormatType)
RCPP_MODULE(EnumMatrixFormatType){
    using namespace Rcpp ;

    class_<EnumMatrixFormatType>("EnumMatrixFormatType")
        .constructor()
        .property("p", &EnumMatrixFormatType::getP, &EnumMatrixFormatType::setP)
    ;
}

RCPP_MODULE(BNGPU_module) {
    class_<BNGPU>( "BNGPU" )
    .constructor()
    .method( "addDataTable", &BNGPU::addDataTable )
    ;
}

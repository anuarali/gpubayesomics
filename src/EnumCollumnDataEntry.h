#ifndef ENUM_COLLUMN_DATA_ENTRY_H
#define ENUM_COLLUMN_DATA_ENTRY_H

#include "enumBase.h"

enum CollumnDataEntry{
    GENE = 0,
    MI_RNA   = 1,
    CIRC_RNA = 2,
    SAMPLE = 3,
    QUANTITY = 4
};

typedef EnumBaseWrapper<CollumnDataEntry> EnumCollumnDataEntry;


#endif//ENUM_COLLUMN_DATA_ENTRY_H

#ifndef ENUM_MATRIX_FORMAT_TYPE
#define ENUM_MATRIX_FORMAT_TYPE

#include "enumBase.h"

enum MatrixFormatType
{
    ZERO_ONE_INCIDENCE_MATRIX = 0,
    ONLY_EDGE_LIST = 1
};

typedef EnumBaseWrapper<MatrixFormatType> EnumMatrixFormatType;


#endif//ENUM_MATRIX_FORMAT_TYPE

#ifndef BNGPU_H
#define BNGPU_H
#include "EnumCollumnDataEntry.h"
#include "EnumMatrixFormatType.h"
#include "Rcpp.h"

class BNGPU {
public:
    BNGPU() {}

    SEXP addDataTable(
            const SEXP input,
            const CollumnDataEntry row_type,
            const CollumnDataEntry col_type,
            const MatrixFormatType matrix_type
    );

};


#endif//BNGPU_H
